import HomeController from './HomeController';

var moduleName='dsht.home';

angular.module(moduleName, [])
    .controller('dsht.homeController', HomeController)

export default moduleName;