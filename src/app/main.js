import { default as mainModuleName   } from './main.module'  ;
import { default as homeModuleName   } from './home.module'  ;
import { default as imagesModuleName } from './images.module';

var moduleName = 'dashTestApp';

function config( $routeProvider ){
    $routeProvider
        .when('/',{
            templateUrl:'htm/home.htm',
            controller:'dsht.homeController',
            controllerAs:'vm'
        })
        .when('/images',{
            templateUrl:'htm/images.htm'
        })
        .otherwise({redirectTo:'/'});
}

config.$inject = ['$routeProvider'];

var app = angular.module(
    moduleName, [
        'ngRoute',
        'ngMessages',
         mainModuleName,
         homeModuleName,
         imagesModuleName
    ]
).config(config);

export default moduleName;