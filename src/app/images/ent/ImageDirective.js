const WM_link = new WeakMap();
const M_heights = new Map();
const FIX_WIDTH = 300;
const MIN_HEIGHT = 150;
const MAX_HEIGHT = 600;

class ImageDirective
{
    constructor(){
        this.require     = ['^^dshtImages'];
        this.restrict    = 'E';
        this.scope       = {vm:'='};
        this.replace     = true;
        this.templateUrl = 'htm/dsht-image.htm';
        this.link = ( scope, elem, attrs, imagesController )=> {        
            elem.css({'background-image': `url("${scope.vm.url}")`});
            elem.width(FIX_WIDTH);
            if( M_heights.has(scope.vm.url) ){
                elem.height(M_heights.get(scope.vm.url));
            }else{
                elem.height(FIX_WIDTH);
                let image = angular.element(`<img src="${scope.vm.url}"/>`);    
                angular.element(image).bind('load',(event)=>{
                    let normalizedHeight = Math.ceil(event.currentTarget.height * FIX_WIDTH / event.currentTarget.width);

                    if( normalizedHeight < MIN_HEIGHT )
                        normalizedHeight = MIN_HEIGHT;
                    else if( normalizedHeight > MAX_HEIGHT )
                        normalizedHeight = MAX_HEIGHT;

                    M_heights.set(event.currentTarget.src, normalizedHeight);
                    elem.height(normalizedHeight);
                    angular.element(event.currentTarget).unbind('load');
                });
            }
        }
    }

    toggleImage( image ){
        this.imagesSet.has(image) ? this.imagesSet.delete(image) : this.imagesSet.add(image);
        this.imagesSelected = this.imagesSet.toJSON();
    }

    static factory( $q ){
        ImageDirective.instance = new ImageDirective();
        return ImageDirective.instance;
    }
}

export default ImageDirective;