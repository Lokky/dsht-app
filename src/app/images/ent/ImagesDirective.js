const WM_$q = new WeakMap();
const WM_$window = new WeakMap();

class ImagesDirective
{
    constructor($q, $window, statsService){
        WM_$q.set(this, $q);
        WM_$window.set(this, $window);
        this.restrict     = 'E';
        this.controller   = 'dsht.imagesController';
        this.controllerAs = 'vm';
        this.templateUrl  = 'htm/dsht-images.htm';
        this.replace      = true;
        this.link = ( scope, elem, attrs, imagesController )=> {
            this.linked = { scope, elem, attrs, imagesController };
            this.tilesContainer = elem.find('.tiles-container')[0];
            this.checkCols();
            this.checkRows();
        };
        angular.element($window).bind('scroll', () => this.checkRows() );
        angular.element($window).bind('resize', () => this.checkCols() );
    }

    checkCols(){
        let columnsCount = Math.floor(this.tilesContainer.clientWidth/320)
        this.linked.imagesController.placeImages(columnsCount);
        this.checkRows();
    }

    checkRows(){
        let cliHeight = WM_$window.get(this).document.documentElement.clientHeight;
        let boundRect = this.tilesContainer.getBoundingClientRect();
        if( boundRect.height + boundRect.top < cliHeight * 1.4 ){
            this.linked.imagesController.reserveImages(() => this.checkRows());
        }
    }

    toggleImage( image ){
        this.imagesSet.has(image) ? this.imagesSet.delete(image) : this.imagesSet.add(image);
        this.imagesSelected = this.imagesSet.toJSON();
    }

    static factory( $q, $window, statsService ){
        ImagesDirective.instance = new ImagesDirective($q, $window, statsService);
        return ImagesDirective.instance;
    }
}

ImagesDirective.factory.$inject = ['$q', '$window', 'dshtStatsService'];

export default ImagesDirective;