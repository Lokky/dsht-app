class StatsService
{
    constructor(){
        this._totalImages = 0
    }

    set imagesTotalCount( imagesTotalCount ){ this._totalImages = imagesTotalCount }
    get imagesTotalCount() { return this._totalImages }

    static factory(){
        return new StatsService();
    }
}

export default StatsService;