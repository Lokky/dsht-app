const WM_http = new WeakMap();
const API_HOST_DEF = '__api_host__';
const API_HOST = API_HOST_DEF != '__api_host__' ? API_HOST_DEF : 'localhost:8081'

class ImagesService
{
    constructor( $http )
    {
        WM_http.set(this, $http);
    }

    getImages(){
        return WM_http.get(this).get(`http://${API_HOST}/api/v1/images`).then( result => result.data );
    }

    getReserve(lastImage){
        return WM_http.get(this).get(`http://${API_HOST}/api/v1/imagesChunk/${lastImage ? lastImage.name : 0}`).then(
            result        => result.data,
            errorResponse => { alert(`Images loading status: ${errorResponse.status} ${errorResponse.statusText}`); }
        );
    }

    static factory( $http ){
        return new ImagesService($http);
    }
}

ImagesService.factory.$inject = ['$http'];

export default ImagesService;