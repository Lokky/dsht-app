const WM_$timeout = new WeakMap();
const WM_service  = new WeakMap();

class ImagesController{
    constructor( $rootScope, $timeout, dshtImagesService, dshtStatsService ){
        this.dshtStatsService = dshtStatsService;
        this.columnsCount = 3;
        this.isReserving = false;
        this.isExhausted = false;
        this.imagesSet = new Set();
        this.images = []; //[{"name":"img1","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img1.jpg"},{"name":"img2","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img2.jpg"},{"name":"img3","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img3.jpg"},{"name":"img4","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img4.jpg"},{"name":"img5","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img5.jpg"},{"name":"img6","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img6.jpg"},{"name":"img7","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img7.jpg"},{"name":"img8","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img8.jpg"},{"name":"img9","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img9.jpg"},{"name":"img10","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img10.jpg"},{"name":"img11","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img11.jpg"},{"name":"img12","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img12.jpg"},{"name":"img13","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img13.jpg"},{"name":"img14","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img14.jpg"},{"name":"img15","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img15.jpg"},{"name":"img16","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img16.jpg"},{"name":"img17","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img17.jpg"},{"name":"img18","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img18.jpg"},{"name":"img19","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img19.jpg"},{"name":"img20","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img20.jpg"}];//,{"name":"img21","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img21.jpg"},{"name":"img22","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img22.jpg"},{"name":"img23","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img23.jpg"},{"name":"img24","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img24.jpg"},{"name":"img25","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img25.jpg"},{"name":"img26","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img26.jpg"},{"name":"img27","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img27.jpg"},{"name":"img28","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img28.jpg"},{"name":"img29","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img29.jpg"},{"name":"img30","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img30.jpg"},{"name":"img31","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img31.jpg"},{"name":"img32","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img32.jpg"},{"name":"img33","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img33.jpg"},{"name":"img34","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img34.jpg"},{"name":"img35","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img35.jpg"},{"name":"img36","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img36.jpg"},{"name":"img37","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img37.jpg"},{"name":"img38","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img38.jpg"},{"name":"img39","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img39.jpg"},{"name":"img40","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img40.jpg"},{"name":"img41","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img41.jpg"},{"name":"img42","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img42.jpg"},{"name":"img43","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img43.jpg"},{"name":"img44","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img44.jpg"},{"name":"img45","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img45.jpg"},{"name":"img46","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img46.jpg"},{"name":"img47","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img47.jpg"},{"name":"img48","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img48.jpg"},{"name":"img49","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img49.jpg"},{"name":"img50","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img50.jpg"},{"name":"img51","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img51.jpg"},{"name":"img52","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img52.jpg"},{"name":"img53","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img53.jpg"},{"name":"img54","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img54.jpg"},{"name":"img55","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img55.jpg"},{"name":"img56","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img56.jpg"},{"name":"img57","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img57.jpg"},{"name":"img58","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img58.jpg"},{"name":"img59","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img59.jpg"},{"name":"img60","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img60.jpg"},{"name":"img61","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img61.jpg"},{"name":"img62","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img62.jpg"},{"name":"img63","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img63.jpg"},{"name":"img64","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img64.jpg"},{"name":"img65","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img65.jpg"},{"name":"img66","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img66.jpg"},{"name":"img67","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img67.jpg"},{"name":"img68","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img68.jpg"},{"name":"img69","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img69.jpg"},{"name":"img70","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img70.jpg"},{"name":"img71","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img71.jpg"},{"name":"img72","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img72.jpg"},{"name":"img73","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img73.jpg"},{"name":"img74","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img74.jpg"},{"name":"img75","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img75.jpg"},{"name":"img76","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img76.jpg"},{"name":"img77","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img77.jpg"},{"name":"img78","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img78.jpg"},{"name":"img79","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img79.jpg"},{"name":"img80","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img80.jpg"},{"name":"img81","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img81.jpg"},{"name":"img82","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img82.jpg"},{"name":"img83","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img83.jpg"},{"name":"img84","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img84.jpg"},{"name":"img85","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img85.jpg"},{"name":"img86","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img86.jpg"},{"name":"img87","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img87.jpg"},{"name":"img88","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img88.jpg"},{"name":"img89","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img89.jpg"},{"name":"img90","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img90.jpg"},{"name":"img91","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img91.jpg"},{"name":"img92","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img92.jpg"},{"name":"img93","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img93.jpg"},{"name":"img94","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img94.jpg"},{"name":"img95","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img95.jpg"},{"name":"img96","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img96.jpg"},{"name":"img97","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img97.jpg"},{"name":"img98","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img98.jpg"},{"name":"img99","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img99.jpg"},{"name":"img100","url":"https://s3-eu-west-1.amazonaws.com/dashdesign/links/test/img100.jpg"}];
        this.imagesColumns = [];
        this.apply = () => { if( $rootScope.$$phase != '$apply' && $rootScope.$$phase != '$digest' ) $rootScope.$apply() }
        WM_service.set(this, dshtImagesService);
        WM_$timeout.set(this, $timeout);
    }

    reserveImages(cb){
        if( this.isReserving || this.isExhausted )return;
        this.isReserving = true;
        let lastImage = this.images[this.images.length-1];
        WM_service.get(this).getReserve(lastImage).then( images => {
            if( images.length < 10 ) this.isExhausted = true;
            this.images = this.images.concat(images);
            this.placeImages();
            this.isReserving = false;
            if( cb )
                 WM_$timeout.get(this)(() => cb(lastImage));
        });
    }

    placeImages( columnsCount, anchorImage ){
        this.dshtStatsService.imagesTotalCount = this.images.length;
        if( columnsCount )
            this.columnsCount = columnsCount;
        else
            columnsCount = this.columnsCount;

        if( !this.images )return;
        this.imagesColumns.length = 0;
        let anchorIndex = anchorImage ? this.images.indexOf(anchorImage) : 0;
        let maxIndex = this.images.length - 1;
        let colIndex = 0;
        for( let imgIndex = anchorIndex; imgIndex <= maxIndex; imgIndex++ ){
            if( columnsCount <= colIndex ) colIndex = 0;
            if( !this.imagesColumns[colIndex] ) this.imagesColumns[colIndex] = [];
            this.imagesColumns[colIndex++].push(this.images[imgIndex])
        }
        this.apply();
    }

    toggleImage( image ){
        this.imagesSet.has(image) ? this.imagesSet.delete(image) : this.imagesSet.add(image);
        this.imagesSelected = this.imagesSet.toJSON();
    }
}

ImagesController.$inject = ['$rootScope', '$timeout', 'dshtImagesService', 'dshtStatsService'];

export default ImagesController;