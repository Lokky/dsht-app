import ImagesController from './ImagesController';
import ImagesService    from './ImagesService'   ;
import ImagesDirective  from './ImagesDirective' ;
import ImageDirective   from './ImageDirective'  ;

var moduleName='dsht.images';

angular.module(moduleName, [])
    .controller( 'dsht.imagesController', ImagesController        )
    .factory   ( 'dshtImagesService'    , ImagesService.factory   )
    .directive ( 'dshtImages'           , ImagesDirective.factory )
    .directive ( 'dshtImage'            , ImageDirective.factory  )

export default moduleName;