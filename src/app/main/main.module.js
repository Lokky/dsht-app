import MainController from './MainController';
import StatsService   from './StatsService'  ;

var moduleName='dsht.main';

angular.module(moduleName, [])
    .factory   ( 'dshtStatsService'   , StatsService.factory    )
    .controller( 'dsht.mainController', MainController          );

export default moduleName;