class MainController{
    constructor(statsService){
        this.stats = statsService;
    }
}

MainController.$inject = [ 'dshtStatsService' ];

export default MainController;